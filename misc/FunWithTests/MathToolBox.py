class MathToolBox:
	"""Class for math functions"""	
	def __init__(self):
		pass

	def __str__(self):
		return "MathToolBox - Class for math functions"

	def caching(func):
		"""Caching decorator to reuse already calculated fibonacci numbers"""
		cached = {}
		def wrapper(*args):
			if args in cached:
				return cached[args]
			else:
				fibNum = func(*args)
				cached[args] = fibNum
				return fibNum
		return wrapper

	@caching
	def fib(self, number):
		"""Method to calculate fibonacci number"""
		if number > 1:
			return self.fib(number-1) + self.fib(number-2)
		return number

if __name__=="__main__":
	pass
