import MathToolBox as mtb

def test_fib():
	m = mtb.MathToolBox()
	assert m.fib(4) == 3 
	assert m.fib(54) == 102334155 

if __name__=="__main__":
	pass
