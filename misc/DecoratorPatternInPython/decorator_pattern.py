#!/usr/bin/env python

"""
Beispiel: Dekorierer in Python
"""

import abc
import math as m
import time as t

#https://docs.python.org/3/library/abc.html

class BasisKlasse(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def methode(self, parameter):
        pass

class KonkreteKlasse(BasisKlasse):

    def methode(self, parameter):
       return m.factorial(parameter)

class Dekorierer(BasisKlasse, metaclass=abc.ABCMeta):

    def __init__(self, konkreteKlasse):
        self.konkreteKlasse = konkreteKlasse

    @abc.abstractmethod
    def methode(self, parameter):
        pass

class Cache(Dekorierer):

    cached = {}

    def caching(self, parameter):
        if parameter in self.cached:
            return self.cached[parameter]
        else:
            self.cached[parameter] = self.konkreteKlasse.methode(parameter)
            return self.cached[parameter]


    def methode(self, parameter):
        return self.caching(parameter)

class Profile(Dekorierer):

    def profile(self, parameter):
        t0 = t.time()
        self.konkreteKlasse.methode(parameter)
        t1 = t.time()
        return "Ausführungszeit: {:.2f} Sekunden".format(t1 - t0)

    def methode(self, parameter):
        return self.profile(parameter)


def main():
    object = KonkreteKlasse()
    parameter = 1111111

    print("\nMethodenaufruf ohne Cache:")
    profiledObject = Profile(object)
    print(profiledObject.methode(parameter))
    print(profiledObject.methode(parameter))

    print("\nMethodenaufruf mit Cache:")
    cachedObject = Cache(object)
    profiledObject = Profile(cachedObject)
    print(profiledObject.methode(parameter))
    print(profiledObject.methode(parameter))

if __name__ == "__main__":
    main()