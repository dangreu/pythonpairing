#!/usr/bin/python3

import re

class PasswordCheck:

    def __init__(self, passwordString):
        self.passwordString = passwordString 
        self.checks={'length': r'.{8,}',
                'upper': r'[A-Z]',
                'lower': r'[a-z]',
                'number': r'[0-9]'}

    def getPasswordString(self):
        return self.passwordString

    def check(self):
        for key, value in self.checks.items():
            newRegex = re.compile(value)
            regResult = newRegex.search(self.passwordString)
            if regResult == None:
                raise ValueError('Password check failed during {} test'.format(key))

    def ngStrip(self, stripChar=r'\s+'):
        regexString = "^{}".format(stripChar, stripChar)
        regexString2 = "{}$".format(stripChar, stripChar)
        newRegex = re.compile(regexString)
        resultString = newRegex.sub('', self.passwordString)
        newRegex2 = re.compile(regexString2)
        resultString = newRegex2.sub('', resultString)
        return resultString

if __name__=='__main__':
    pass

