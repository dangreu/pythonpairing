import QuizMaster as qm

print("Hello! Welcome to the quizshow")
qm = qm.QuizMaster()
activeQuiz=True
while activeQuiz:
    qm.createQuestion()
    print(qm.printQuestion())
    answer = ''
    while answer not in ['A', 'B', 'x']:
        answer = input("Deine Antwort (x für beenden): ")
    if answer == 'x':
        activeQuiz = False
    else:
        qm.validateAnswer(answer)
qm.showScore()
