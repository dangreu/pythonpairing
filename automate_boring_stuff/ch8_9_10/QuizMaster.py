import json, random

class QuizMaster:

    def __init__(self, questions='questions.json'):
        with open(questions) as fileObj:
            self.questions = json.load(fileObj)['data']
        self.correctAnswer = 0
        self.questionCount = 0


    def createQuestion(self):
        randomQuestionList = random.sample(self.questions, 2)

        answerList = [
                    randomQuestionList[0]['answer'], 
                    randomQuestionList[1]['answer']
                ]
        random.shuffle(answerList)
        A, B = answerList
        if randomQuestionList[0]['answer'] == A:
            correctAnswer = "A"
        else:
            correctAnswer = "B"

        self.currentQuestion = {
                "question": randomQuestionList[0]['state'],
                "A": A,
                "B": B,
                "correctAnswer": correctAnswer
                }
    
        return self.currentQuestion

    def printQuestion(self):
        return ("\n\nWas ist mit {}.\n Antwortmöglichkeiten \n A) {}\n B) {}".format(self.currentQuestion['question'], self.currentQuestion['A'], self.currentQuestion['B']))

    def validateAnswer(self, answer):
        result = self.currentQuestion['correctAnswer'] == answer
        if result:
            self.correctAnswer += 1
        self.questionCount += 1
        self.feedback(result)

    def feedback(self, result):
        if result:
            print("====> Congrats!\n")
        else:
            print("====> You Loser!\n")
            print("Correct answer: {}".format(
                self.currentQuestion['correctAnswer']
            ))

    def showScore(self):
        score = self.correctAnswer/self.questionCount*100
        print("Your score: {first:.2f} %".format(first=score))

if __name__=="__main__":
    pass
