import hashlib as hlib
import json
from flask import Flask, jsonify, request

class BlockChain:

    def __init__(self, hashLibName):
        self.chain = []
        self.setHashMethod(hashLibName)
        self.createBlock(nounce = 1, previousHash = '0')
        pass

    def setHashMethod(self, hashName):
        if hashName == 'sha256':
            self.hashMethod = hlib.sha256
        elif hashName == 'sha512':
            self.hashMethod = hlib.sha512
        else:
            raise Exception('Invalid hash method name!')
    
    def createBlock(self, nounce, previousHash, data = {}):
        block = {
                'index': len(self.chain) - 1,
                'nounce': nounce,
                'previousHash': previousHash,
                'data': data
                }
        self.chain.append(block)
    
    def hashOperation(self, strToHash):
        if not isinstance(strToHash, str):
            raise Exception('Not a valid string to hash')
        return self.hashMethod(strToHash.encode()).hexdigest()

    def miningOperation(self, nounce, previousNounce):
        #TODO symm
        return str(nounce**2 - previousNounce**3)

    def mineBlock(self, nounce, previousNounce):
        #TODO check why encode important
        hashValue = self.hashOperation(self.miningOperation(nounce, previousNounce))
        if hashValue[:4] == '0000': 
            return True
        return False

    def hashBlock(self, block):
        stringToHash = json.dumps(block, sort_keys = True)
        hashValue = self.hashOperation(stringToHash)
        return hashValue

    def checkChain(self, chain):
        for blockIdx in xrange(1,len(self.chain)):
            block = self.chain[blockIdx]
            previousBlock = self.chain[blockIdx - 1]

            if block['previousHash'] != self.hashBlock(previousBlock):
                return False
        return True

    def getPreviousBlock(self):
        return self.chain[-1]
            

app = Flask(__name__)

blockchain = BlockChain('sha256')

@app.route('/mine_block', methods = ['GET'])
def mine_block():
    userData = request.args.get('user_data')
    money = request.args.get('user_money')
    previousBlock = blockchain.getPreviousBlock()
    nounce = 2 
    previousNounce = previousBlock['nounce']
    while not blockchain.mineBlock(nounce, previousNounce):
        nounce += 1
    block = blockchain.createBlock(nounce, blockchain.hashBlock(previousBlock), {'user_data': userData, 'money': money})

    return jsonify({'message': 'passt!'}), 200


@app.route('/get_chain', methods = ['GET'])
def get_chain():
    response = {
        'chain': blockchain.chain,
        'length': len(blockchain.chain)
            }
    return jsonify(response), 200

app.run(host = '0.0.0.0', port = 5000)
